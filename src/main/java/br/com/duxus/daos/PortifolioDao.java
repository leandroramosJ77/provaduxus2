package br.com.duxus.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import br.com.duxus.models.PaginatedList;
import br.com.duxus.models.Portifolio;


@Repository
public class PortifolioDao {

	   @PersistenceContext
	   private EntityManager manager;

	   public List<Portifolio> all()
	   {
	      return manager.createQuery("select p from Portifolio p", Portifolio.class).getResultList();
	   }

	   public void save(Portifolio portifolio)
	   {
	      manager.persist(portifolio);
	   }

	   public Portifolio findById(Integer id)
	   {
	      return manager.find(Portifolio.class, id);
	   }

	   public void remove(Portifolio portifolio)
	   {
	      manager.remove(portifolio);
	   }

	   public void update(Portifolio portifolio)
	   {
	      manager.merge(portifolio);
	   }

	   public PaginatedList paginated(int page, int max)
	   {
	      return new PaginatorQueryHelper().list(manager, Portifolio.class, page, max);
	   }
}
