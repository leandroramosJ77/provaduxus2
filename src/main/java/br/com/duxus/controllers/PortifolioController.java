package br.com.duxus.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;



import br.com.duxus.models.Portifolio;
import br.com.duxus.daos.CategoryDao;
import br.com.duxus.daos.PortifolioDao;

@Controller
@RequestMapping("/portifolio")
@Transactional
public class PortifolioController {
	
	   @Autowired
	   private PortifolioDao portifolioDao;
	   
	   @Autowired
	   private CategoryDao categoryDao;
	 
	   @GetMapping("/form")
	   public ModelAndView form(Portifolio portifolio)
	   {
		   System.out.println("Form !!");
	      ModelAndView modelAndView = new ModelAndView("portifolio/form-add");
	      return loadFormDependencies(modelAndView);

	   }

	   private ModelAndView loadFormDependencies(ModelAndView modelAndView)
	   {
	     // modelAndView.addObject("categoryList", portifolioDao.all());
	      modelAndView.addObject("categoryList", categoryDao.all());
	      return modelAndView;
	   }

	   @PostMapping
	   public ModelAndView save(@Valid Portifolio portifolio, BindingResult bindingResult)
	   {
	      if (bindingResult.hasErrors())
	      {
	         return form(portifolio);
	      }
	      portifolioDao.save(portifolio);
	      return new ModelAndView("redirect:/portifolio");
	   }

	   @GetMapping("/{id}")
	   public ModelAndView load(@PathVariable("id") Integer id)
	   {
	      ModelAndView modelAndView = new ModelAndView("portifolio/form-update");
	      modelAndView.addObject("portifolio", portifolioDao.findById(id));
	      loadFormDependencies(modelAndView);
	      return modelAndView;
	   }

	   @RequestMapping(method = RequestMethod.GET)
	   public ModelAndView list(@RequestParam(defaultValue = "0", required = false) int page)
	   {
		  // ModelAndView modelAndView = new ModelAndView("home/index");
		   System.out.println("Mapping !!");
	      ModelAndView modelAndView = new ModelAndView("portifolio/list");
	    //  System.out.println(portifolioDao.all());
	      modelAndView.addObject("paginatedList", portifolioDao.paginated(page, 10));
	      return modelAndView;
	      
	   }

	   //just because get is easier here. Be my guest if you want to change.
	   @GetMapping("/remove/{id}")
	   public String remove(@PathVariable("id") Integer id)
	   {
	      Portifolio portifolio = portifolioDao.findById(id);
	      portifolioDao.remove(portifolio);
	      return "redirect:/portifolio";
	   }

	   @PostMapping("/{id}")
	   public ModelAndView update(@PathVariable("id") Integer id, @Valid Portifolio portifolio, BindingResult bindingResult)
	   {
		   portifolio.setId(id);
	      if (bindingResult.hasErrors())
	      {
	         return loadFormDependencies(new ModelAndView("portifolio/form-update"));
	      }
	      portifolioDao.update(portifolio);
	      return new ModelAndView("redirect:/portifolio");
	   }
}
