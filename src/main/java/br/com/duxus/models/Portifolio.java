package br.com.duxus.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import java.math.BigDecimal;
import br.com.duxus.models.Category;
@Entity
public class Portifolio 
{
	
	   @Id
	   @GeneratedValue(strategy = GenerationType.IDENTITY)
	   private Integer id;
	   private String apelidoprojeto;
	   private String nomecliente;
	   private String descricao;
	   private String screenshot;	   
	   private Boolean status;
	   @ManyToOne
	   private Category category;
	   
	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getApelidoprojeto() {
		return this.apelidoprojeto;
	}
	
	public void setApelidoprojeto(String apelidoprojeto) {
		this.apelidoprojeto = apelidoprojeto;
	}
	
	public String getNomecliente() {
		return this.nomecliente;
	}
	
	public void setNomecliente(String nomecliente) {
		this.nomecliente = nomecliente;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getScreenshot() {
		return this.screenshot;
	}
	
	public void setScreenshot(String screenshot) {
		this.screenshot = screenshot;
	}
	
	public Boolean getStatus() {
		return this.status;
	}
	
	public void setStatus(Boolean status) {
		this.status = status;
	}

	   public Category getCategory()
	   {
	      return this.category;
	   }

	   public void setCategory(Category category)
	   {
	      this.category = category;
	   }
}
