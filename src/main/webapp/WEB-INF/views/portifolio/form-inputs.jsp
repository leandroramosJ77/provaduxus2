<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  	        <div class="form-group">
          <label for="apelidoprojeto">apelido projeto</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-unchecked"></i></span><form:input path='apelidoprojeto' type='text'/>
<form:errors path='apelidoprojeto'/>

          </div>
        </div>
        <div class="form-group">
          <label for="nomecliente">nome cliente</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-unchecked"></i></span><form:input path='nomecliente' type='text'/>
<form:errors path='nomecliente'/>

          </div>
        </div>
        <div class="form-group">
          <label for="price">descricao</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-unchecked"></i></span><form:input path='descricao' type='text'/>
<form:errors path='descricao'/>

          </div>
        </div>
        
        
        <div class="form-group">
          <label for="screenshot">screenshot</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-unchecked"></i></span>
            <form:input path='screenshot' type='text'/>
			<form:errors path='screenshot'/>

          </div>
        </div>        
        
        
        <div class="form-group">
          <label for="category.id">category.id</label>
          <div class="input-group">
            <span class="input-group-addon">
            	<i class="glyphicon glyphicon-unchecked"></i>
            </span>
            <form:select path='category.id' items='${categoryList}' itemValue='id' itemLabel='name'>
			</form:select>
<form:errors path='category.id'/>

          </div>
        </div>
